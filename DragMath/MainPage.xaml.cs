﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace DragMath
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            double et, hp, mph, weight;
            bool isNum1, isNum2;
            string text1, text2;
            text1 = textBox1.Text;
            text2 = textBox2.Text;

            isNum1 = double.TryParse(text1, out hp);
            if (!isNum1) { MessageBox.Show("Please enter a valid number for Horsepower"); }

            isNum2 = double.TryParse(text2, out weight);
            if (!isNum2) { MessageBox.Show("Please enter a valid number for Car weight"); }

            if (isNum1 && isNum2) {

                //et Formula: (weight/hp)^0.2574 x 7.3571
                et = Math.Pow((weight / hp), 0.2574) * 7.3571;
                et = Math.Round(et, 2);

                //mph Formula: (hp/weight)^0.3018 x 215.39
                mph = Math.Pow((hp / weight), 0.3018) * 215.39;
                mph = Math.Round(mph, 2);
                textBox3.Text = "1/4 Mile ET: " + et.ToString() + " seconds @ " + mph.ToString() + "mph";
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            double hp, et, weight;
            bool isNum1, isNum2;
            string text1, text2;
            text1 = textBox4.Text;
            text2 = textBox5.Text;

            isNum1 = double.TryParse(text1, out et);
            if (!isNum1) { MessageBox.Show("Please enter a valid number for ET"); }

            isNum2 = double.TryParse(text2, out weight);
            if (!isNum2) { MessageBox.Show("Please enter a valid number for Car weight"); }

            if (isNum1 && isNum2)
            {
                //HP Formula: weight / ((et / 7.3571)^3.88500389)
                hp = weight / Math.Pow((et / 7.3571), 3.88500389);
                hp = Math.Round(hp, 0);
                textBox6.Text = "HP: " + hp.ToString();
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            double hp, mph, weight;
            bool isNum1, isNum2;
            string text1, text2;
            text1 = textBox7.Text;
            text2 = textBox8.Text;

            isNum1 = double.TryParse(text1, out mph);
            if (!isNum1) { MessageBox.Show("Please enter a valid number for MPH"); }

            isNum2 = double.TryParse(text2, out weight);
            if (!isNum2) { MessageBox.Show("Please enter a valid number for Car weight"); }

            if (isNum1 && isNum2)
            {
                //HP Formula: (mph / 215.39)^3.313452618) * weight
                hp = weight * Math.Pow((mph / 215.39), 3.313452618);
                hp = Math.Round(hp, 0);
                textBox9.Text = "HP: " + hp.ToString();
            }
        }

        private void textBox1_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = "";
        }

        private void textBox1_lostFocus(object sender, RoutedEventArgs e)
        {
            double temp;
            bool isNum = double.TryParse(textBox1.Text, out temp);
            if (!isNum) { textBox1.Text = "Horsepower"; }
        }

        private void textBox2_Click(object sender, RoutedEventArgs e)
        {
            textBox2.Text = "";
        }

        private void textBox2_lostFocus(object sender, RoutedEventArgs e)
        {
            double temp;
            bool isNum = double.TryParse(textBox2.Text, out temp);
            if (!isNum) { textBox2.Text = "Car Weight"; }
        }

        private void textBox4_Click(object sender, RoutedEventArgs e)
        {
            textBox4.Text = "";
        }

        private void textBox4_lostFocus(object sender, RoutedEventArgs e)
        {
            double temp;
            bool isNum = double.TryParse(textBox4.Text, out temp);
            if (!isNum) { textBox4.Text = "ET"; }
        }

        private void textBox5_Click(object sender, RoutedEventArgs e)
        {
            textBox5.Text = "";
        }

        private void textBox5_lostFocus(object sender, RoutedEventArgs e)
        {
            double temp;
            bool isNum = double.TryParse(textBox5.Text, out temp);
            if (!isNum) { textBox5.Text = "Car Weight"; }
        }

        private void textBox7_Click(object sender, RoutedEventArgs e)
        {
            textBox7.Text = "";
        }

        private void textBox7_lostFocus(object sender, RoutedEventArgs e)
        {
            double temp;
            bool isNum = double.TryParse(textBox7.Text, out temp);
            if (!isNum) { textBox7.Text = "MPH"; }
        }

        private void textBox8_Click(object sender, RoutedEventArgs e)
        {
            textBox8.Text = "";
        }

        private void textBox8_lostFocus(object sender, RoutedEventArgs e)
        {
            double temp;
            bool isNum = double.TryParse(textBox8.Text, out temp);
            if (!isNum) { textBox8.Text = "Car Weight"; }
        }
    }
}
